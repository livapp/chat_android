package org.thoughtcrime.securesms;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class SetDefaultSmsDialog extends AlertDialog {

  public SetDefaultSmsDialog(@NonNull Context context) {
    super(context);
  }

  public static void show(Context context, Runnable runnable) {
    AlertDialog.Builder builder = new MaterialAlertDialogBuilder(context);
    builder.setTitle(R.string.SetDefaultSmsDialog__title)
           .setMessage(R.string.SetDefaultSmsDialog__message)
           .setCancelable(true)
           .setPositiveButton(R.string.SetDefaultSmsDialog__positive, (d, w) -> {
             if (runnable != null) runnable.run();
           })
           .setNegativeButton(R.string.SetDefaultSmsDialog__negative, null);

    builder.show();
  }
}
