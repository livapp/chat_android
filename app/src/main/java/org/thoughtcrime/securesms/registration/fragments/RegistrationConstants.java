package org.thoughtcrime.securesms.registration.fragments;

final class RegistrationConstants {

  private RegistrationConstants() {
  }

  static final String TERMS_AND_CONDITIONS_URL = "https://static.privachatapp.com/policies/privacy_tos.html";
  static final String SIGNAL_CAPTCHA_URL       = "https://signalcaptchas.org/registration/generate.html";
  static final String SIGNAL_CAPTCHA_SCHEME    = "signalcaptcha://";

}
